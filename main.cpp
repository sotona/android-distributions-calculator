#include <QApplication>
#include <QQmlApplicationEngine>

//#include <QQuickStyle>


#include "calc.h"


int main(int argc, char *argv[])
{

    // включить поддержку дисцплеев с высоким DPI (опционально)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    qmlRegisterType<Calc>("calc", 1, 0, "Calc");
//    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) return -1;


    return app.exec();
}
