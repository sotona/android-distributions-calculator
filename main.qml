import QtQuick 2.0
import QtQuick.Window 2.2


import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4  // не работает?
import QtQuick.Controls.Material 2.2  // не работает?

import calc 1.0
import QtQuick.Layouts 1.0

import QtCharts 2.2


ApplicationWindow {
    id: window
    visible: true

    minimumWidth: 320
    minimumHeight: 350

    width: 320
    height: 500
    title: qsTr("Нормальное распределение")
    color: "#eeeeee"

    Material.theme: Material.Dark
    Material.accent: Material.BlueGrey


    Calc{
        id: backend
        mean: mean.real_value
        std: std.real_value

        onMeanChanged:   { z_value.value = backend.z * z_value.mx;}
        onStdChanged:    { z_value.value = backend.z * z_value.mx;}
        onZChanged:      { z_value.value = backend.z * z_value.mx;}
        onPvalueChanged: { p_value.value = backend.pvalue * p_value.mx;}
    }

    MouseArea{
        onClicked: chart.forceActiveFocus()
        anchors.fill: parent
    }

    Text {
        id: title
        text: "Normal (Gaussian) distribution"
        font.pointSize: 14
        font.italic: true
        anchors.horizontalCenter: parent.horizontalCenter // anchor - привязка, якорь
        anchors.horizontalCenterOffset: 0
        anchors.top: parent.top
        anchors.topMargin: 10
    }


    Text {
        id: p_value_label
        text: qsTr("p")
        anchors.topMargin: 20
        font.pointSize: 16


        anchors {
            horizontalCenter: parent.horizontalCenter;
            top: title.bottom;
            margins: 10; }
    }

    DoubleSpinbox {
        id: p_value
        font.bold: false
        anchors.topMargin: 6
        value: 0.9 * mx
        from: 1
        stepSize: 100
        to: 9999

        anchors {horizontalCenter: parent.horizontalCenter; top: p_value_label.bottom; margins: 10}

        onValueChanged: {
            backend.pvalue = value / mx; }

        Keys.onReturnPressed: {
//            console.log("enter")
            // иначе клавиатура не пропадает...
            chart.forceActiveFocus()}
    }

    Text {
        id: z_value_label
        width: 13
        height: 16
        text: "z"
        font.pointSize: 16
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenterOffset: 0
        anchors.topMargin: 29
        anchors {
            horizontalCenter: parent.horizontalCenter;
            top: p_value.bottom;
            margins: 10; }
    }


    DoubleSpinbox{
        id: z_value
        to: 5000000
        from: -5000000
        anchors.topMargin: 6
        value: 1.2815*mx        // см. p_value.value
        stepSize: 100
        anchors {top: z_value_label.bottom; horizontalCenter: parent.horizontalCenter; margins: 10}

        onValueChanged: {
                backend.z = value / mx;}

        Keys.onReturnPressed: {
//            console.log("enter")
            // иначе клавиатура не пропадает...
            chart.forceActiveFocus()}
    }

    RowLayout {
        x: 13
        y: 245
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: chart.top
        anchors.bottomMargin: 15
        spacing: 50

        ColumnLayout {
            spacing: 2

            Text {
                id: text2
                color: "#666666"
                text: qsTr("mean")
                font.bold: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            DoubleSpinbox {
                id: mean
                width: 135
                font.bold: true
                Layout.preferredHeight: 27
                Layout.preferredWidth: 125
                font.weight: Font.DemiBold
                font.pointSize: 8
                decimals: 3
                from: -1000*mx
                stepSize: 100
                to: 1000*mx
                value: 0
            }
        }

        ColumnLayout {
            id: columnLayout
            spacing: 2

            Text {
                id: text1
                color: "#666666"
                text: qsTr("std")
                font.bold: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            DoubleSpinbox {
                id: std
                width: 135
                font.bold: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredHeight: 27
                Layout.preferredWidth: 125
                font.weight: Font.DemiBold

                decimals: 3
                from: 0.01*mx
                stepSize: 0.1*mx
                to: 1000*mx
                font.pointSize: 8
                value: 1*mx

            }
        }
    }

    ChartView{
        id: chart
        width: 340
        height: 190
        backgroundColor: "#d7eeff"
        anchors.horizontalCenterOffset: 0
        antialiasing: true

        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: -30

        margins.left: 0
        margins.right: 0

        legend.visible: false

        MouseArea{
            anchors.fill: parent

            onDoubleClicked: {mean.value = 0; std.value = 1*std.mx}
        }


        ValueAxis {
            id: axis_x
            gridVisible: false
            lineVisible: true
            labelsVisible: false
            min: -3.5
            max: 3.5
            tickCount: 5
            }

        ValueAxis {
            id: axis_y
            gridVisible: false
            lineVisible: true
            labelsVisible: false
            min:0
            max: 0.4
            tickCount: 0
            }

        Text {
//            z: 100
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: chart.bottom

            id: mean_label
            y: 135
            text: (mean.value/mean.mx).toFixed(2)
            color: "#6b6b6b"
            font.pointSize: 8
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenterOffset: 10
            anchors.bottomMargin: 40
        }



        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: chart.bottom

            id: z_label
            y: 135

            text: (z_value.value/z_value.mx).toFixed(2)

            color: "#6b6b6b"
            font.pointSize: 8
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenterOffset: 85
            anchors.bottomMargin: 39
        }


        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            id: p_label
            y: 135

            text: (p_value.value/p_value.mx).toFixed(2)

            color: "#6b6b6b"
            font.pointSize: 14
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenterOffset: 13
            anchors.bottomMargin: 10
        }

        AreaSeries{
            id: area
            pointLabelsVisible: false
            axisX: axis_x
            axisY: axis_y

            upperSeries: LineSeries{
                XYPoint{ x: -3.50; y:0.00 }
                XYPoint{ x: -3.13; y:0.00 }
                XYPoint{ x: -2.76; y:0.01 }
                XYPoint{ x: -2.39; y:0.02 }
                XYPoint{ x: -2.03; y:0.05 }
                XYPoint{ x: -1.66; y:0.10 }
                XYPoint{ x: -1.17; y:0.20 }
                XYPoint{ x: -0.99; y:0.25 }
                XYPoint{ x: -0.81; y:0.29 }
                XYPoint{ x: -0.63; y:0.33 }
                XYPoint{ x: -0.45; y:0.36 }
                XYPoint{ x: -0.27; y:0.38 }
                XYPoint{ x: -0.09; y:0.395 }
                XYPoint{ x: 0.00; y:0.397 }
                XYPoint{ x: 0.09; y:0.395 }
                XYPoint{ x: 0.27; y:0.38 }
                XYPoint{ x: 0.45; y:0.36 }
                XYPoint{ x: 0.63; y:0.33 }
                XYPoint{ x: 0.81; y:0.29 }
                XYPoint{ x: 0.99; y:0.25 }
                XYPoint{ x: 1.17; y:0.20 }

                XYPoint{ x: 1.66; y:0.10 }
//                XYPoint{ x: 2.03; y:0.05 }
//                XYPoint{ x: 2.39; y:0.02 }
//                XYPoint{ x: 2.76; y:0.01 }
//                XYPoint{ x: 3.13; y:0.00 }
//                XYPoint{ x: 3.50; y:0.00 }

            }
        }

        Component.onCompleted: {}
    }


}
