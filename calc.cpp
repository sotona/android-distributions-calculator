#include "calc.h"
#include <math.h>
#include <random>

double eps = 1.0e-4;

#include <boost/math/distributions/normal.hpp> // для математических вычислений
// boost устанавливается отдельно
// debian: sudo apt install libboost-math-dev
// https://www.boost.org/users/download/ (см. index.htm внутри архива)

using namespace boost::math;

Calc::Calc(QObject *parent) : QObject(parent){
    setPvalue(0.9);
}

double Calc::z() const{
    return this->_z;
}

double Calc::pvalue() const{
    return this->_p;
}


void Calc::setZ(double z1){
    if (fabs(_z - z1) > eps) {
        this->_z = z1;
        this->_p = cdf(normal(m ,sd), _z);
        zChanged();
        pvalueChanged();}

}


void Calc::setPvalue(double p1){
    if (p1 >0 && p1 < 1){
        if (fabs(_p - p1) > eps){
            this->_p = p1;
            this->_z = quantile(normal(m, sd), _p);
            pvalueChanged();
            zChanged();}}
}


double Calc::mean() const{
    return this->m;}


double Calc::std() const{
    return this->sd;}


void Calc::setMean(double mean){
    if (fabs(m - mean) > eps){
        this->m = mean;
        this->_z = quantile(normal(m, sd), _p);
        meanChanged();
        zChanged();}

}

void Calc::setStd(double std){
    if (std > 0){
        if (fabs(sd - std) > eps){
            this->sd = std;
            this->_z = quantile(normal(m, sd), _p);
            stdChanged();zChanged();}}
}
