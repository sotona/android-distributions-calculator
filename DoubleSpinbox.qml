import QtQuick 2.0
import QtQuick.Window 2.2


import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2


SpinBox {
    editable: true
    wheelEnabled: true
    
    property double mx: Math.pow(10, decimals)
    property double real_value: value / mx
    property int decimals: 4

    
    textFromValue: function(value, locale) {
        return Number(value / mx).toLocaleString(locale, 'f', decimals)
    }
    
    valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * mx
    }
}
